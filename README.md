# CrazyEgg

---

[TOC]

## Introduction

Register and initialize crazyegg integration.

## Restrictions

+ One campaign per page

+ No more than 10 experiences

  (You can have more than 10 experiences, but in this case please be sure that you covered all them in CrazyEgg UI taking snapshots. Please read more in __Deployment Instructions__ section)

+ No support for redirect campaigns

+ No support for campaigns inside the Master campaign (please contact SE Team for custom investigation in case you can't get rid of the Master)

## Download

* [crazyegg-register.js](https://bitbucket.org/mm-global-se/if-crazyegg/src/master/src/crazyegg-register.js)

* [crazyegg-initialize.js](https://bitbucket.org/mm-global-se/if-crazyegg/src/master/src/crazyegg-initialize.js)

## Ask client for

+ Campaign name

+ Browser Rules (which devices should be tracked by CrazyEgg)

+ CrazyEgg data collecting Start Date

+ CrazyEgg data collecting End Date

## Deployment Instructions

This integration requires some additional movements

+ Ensure that you have Integration Factory plugin deployed on site level site wide and it has _order: -10_ ([find out more](https://bitbucket.org/mm-global-se/integration-factory))

+ Create another site script (mapped site wide) with _order: -5_ and add the code from [crazyegg-register.js](https://bitbucket.org/mm-global-se/if-crazyegg/src/master/src/crazyegg-register.js) into it

+ Create campaign script with _order: > -5_, then customize the code from [crazyegg-initialize.js](https://bitbucket.org/mm-global-se/if-crazyegg/src/master/src/crazyegg-initialize.js) and add into it

+ Publish target campaign and all your domain changes

### Create snapshot using standart approach (for public accessible pages)

+ Go to [CrazyEgg Dashboard](https://www.crazyegg.com/dashboard)

+ Add a New Snapshot

    ![crazyegg_1.png](https://bitbucket.org/repo/7ezd6b/images/119445871-crazyegg_1.png)

+ Enter Page URL

    ![crazyegg_2.png](https://bitbucket.org/repo/7ezd6b/images/1557173409-crazyegg_2.png)

+ Get Snapshot Name

    * Go to your test page

    * Get value of `window.CE_SNAPSHOT_NAME` variable (use developer console)

+ Add Snapshot Name

    ![crazyegg_3.png](https://bitbucket.org/repo/7ezd6b/images/188991959-crazyegg_3.png)

+ Select "Track By Name" radio button

    ![crazyegg_4.png](https://bitbucket.org/repo/7ezd6b/images/2265837481-crazyegg_4.png)

+ Define Start and End dates of CrazyEgg data collection

    ![crazyegg_5.png](https://bitbucket.org/repo/7ezd6b/images/3141708075-crazyegg_5.png)

+ Repeat last 6 steps above for each possible campaign experience (elements combination)

### Create snapshot of hidden pages using __Page Camera__ (e.g. pages that are behind a login)

+ Login into the CrazyEgg account

+ Go to [https://www.crazyegg.com/extras](https://www.crazyegg.com/extras)

+ Download Page Camera Application and follow installation instructions

![ce_0.png](https://bitbucket.org/repo/7ezd6b/images/1044496680-ce_0.png)

+ Open Page Camera Application (it will open browser like window)

![ce_1.png](https://bitbucket.org/repo/7ezd6b/images/3909145743-ce_1.png)

+ Open Page Camera and navigate to the __page you want to track__. You can type a URL into the address bar near the top of the window, just like in a normal browser. After you type an address, press the enter key.

![ce_4.png](https://bitbucket.org/repo/7ezd6b/images/2070586830-ce_4.png)

+ After your page has been captured, you will be directed back to Crazy Egg to login and __fill out the snapshot creation form__. Use this form to setup your snapshot in the normal way. Specify a name, URL-matching rules, and expiration options.

![ce_6.png](https://bitbucket.org/repo/7ezd6b/images/675537464-ce_6.png)

![ce_7.png](https://bitbucket.org/repo/7ezd6b/images/2796618394-ce_7.png)

## QA

+ In CrazyEgg UI check if snapshots visually equal to corresponding experiences

+ In CrazyEgg UI choose target snapshot after some time to see if data is collecting