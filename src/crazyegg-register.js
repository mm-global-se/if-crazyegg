mmcore.IntegrationFactory.register('CrazyEgg', {
  validate: function (data) {
    if(!data.campaign)
        return 'No campaign.';

    return true;
  },

  exec: function (data) {
    window.CE_SNAPSHOT_NAME = data.campaignInfo;

    if(data.callback) data.callback();
    return true;
  }
});